package com.corerights.service.iface;

import com.corerights.service.bean.Event;

public interface CoreRightsService {
	
	public String saveEvent(Event event);

}
