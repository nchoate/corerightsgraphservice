package com.corerights.service.bean;

import java.util.List;

public interface IVertex {

	public List<Object> getProperties();
	
	public String getVertexName();
	
	public String getId();
	
}
