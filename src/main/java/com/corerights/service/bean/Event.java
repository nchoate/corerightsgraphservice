package com.corerights.service.bean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Event implements IVertex {

	private String id;
	private LocalDateTime created;
	private String content;
	private Coordinate coordinate;
	private Source source;
	private List<Classification> classifications;
	private Party party;
	private List<NamedEntity> entities;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Coordinate getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	public Source getSource() {
		return source;
	}
	public void setSource(Source source) {
		this.source = source;
	}
	public List<Classification> getClassifications() {
		return classifications;
	}
	public void setClassifications(List<Classification> classifications) {
		this.classifications = classifications;
	}
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}
	
	public List<NamedEntity> getEntities() {
		return entities;
	}
	public void setEntities(List<NamedEntity> entities) {
		this.entities = entities;
	}
	public List<Object> getProperties()
	{
		List<Object> props = new ArrayList<>(6);
		props.add("content");
		props.add(getContent());
		props.add("created");
		props.add(getCreated());
		props.add("coordinate");
		props.add(getCoordinate());
		return props;
	}
	
	public String getVertexName() {
	
		return "Event";
	}
	
	
}
