package com.corerights.service.bean;

import java.util.ArrayList;
import java.util.List;

public class Source implements IVertex{


	private String name;

	private Source(String name)
	{

		this.name = name;
	}

	public String getName() {
		return name;
	}



	public static Source Facebook()
	{
		return new Source("Facebook");
	}

	@Override
	public List<Object> getProperties() {
		List<Object> props = new ArrayList<>(2);
		props.add("name");
		props.add(getName());
		return props;
	}

	@Override
	public String getVertexName() {
		return "Source";
	}

	@Override
	public String getId() {
		return getName();
	}
}
