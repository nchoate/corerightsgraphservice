package com.corerights.service.bean;

import java.util.ArrayList;
import java.util.List;

public class NamedEntity implements IVertex {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public List<Object> getProperties() {
		List<Object> props = new ArrayList<>();
		props.add("name");
		props.add(getName());
		return props;
	}

	@Override
	public String getVertexName() {
		return "NamedEntity";
	}

	@Override
	public String getId() {
		return getName();
	}
	
	
}
