package com.corerights.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;

import com.corerights.dao.iface.GraphDao;
import com.corerights.service.bean.Event;
import com.corerights.service.bean.IVertex;
import com.corerights.service.bean.NamedEntity;
import com.corerights.service.bean.Party;
import com.corerights.service.bean.Source;

public class TinkerMemoryDaoImpl implements GraphDao{

	private Graph graph;
	private Map<String, Vertex> sources;
	
	public TinkerMemoryDaoImpl()
	{
		graph = TinkerGraph.open(); 
		sources = new HashMap<>();
		Vertex facebook = graph.addVertex(buildVertexArray(Source.Facebook()));
		sources.put(Source.Facebook().getId(), facebook);
	}
	
	private Object[] buildVertexArray(IVertex vertex)
	{
		List<Object> properties = new ArrayList<>(4);
		properties.add(T.label);
		properties.add(vertex.getVertexName());
		properties.add(T.id);
		properties.add(vertex.getId());
		properties.addAll(vertex.getProperties());
		return properties.toArray();
	}
	
	private Vertex getFirstVertex(Iterator<Vertex> verticies)
	{
		if(verticies == null)
		{
			return null;
		}
		
		if (verticies.hasNext()) 
		{
			Vertex vertex = verticies.next();
			return vertex;
		}
		else
		{
			return null;
		}
	}
	
	public Graph getGraph()
	{
		return this.graph;
	}
	
	public String saveEvent(Event event) {
		String eventId = UUID.randomUUID().toString();
		event.setId(eventId);
		Vertex vEvent = graph.addVertex(buildVertexArray(event));
		
		vEvent.addEdge("source", sources.get(Source.Facebook().getId()));
		
		Vertex party = saveVertex(event.getParty());
		vEvent.addEdge("by",party);
		
		for(NamedEntity ne: event.getEntities())
		{
			Vertex v = saveVertex(ne);
			vEvent.addEdge("ner", v);
		}
		
		return eventId;
	}
	

	public Event getEvent(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Vertex saveVertex(IVertex vert)
	{
		Vertex vertex = getFirstVertex(graph.vertices(vert.getId()));
		if(vertex == null)
		{
			vertex = graph.addVertex(buildVertexArray(vert));
		}
		return vertex;
	}

}
