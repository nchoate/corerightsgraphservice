package com.corerights.dao.iface;

import org.apache.tinkerpop.gremlin.structure.Graph;

import com.corerights.service.bean.Event;
import com.corerights.service.bean.Party;

public interface GraphDao {
	
	public Graph getGraph();
	
	public String saveEvent(Event event);
	
	public Event getEvent(String id);
	
	
	

}
