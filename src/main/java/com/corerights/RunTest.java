package com.corerights;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.tinkerpop.gremlin.structure.Direction;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;

import com.corerights.dao.iface.GraphDao;
import com.corerights.dao.impl.TinkerMemoryDaoImpl;
import com.corerights.service.bean.Coordinate;
import com.corerights.service.bean.Event;
import com.corerights.service.bean.NamedEntity;
import com.corerights.service.bean.Party;
import com.corerights.service.bean.Source;

public class RunTest {

	public static void main(String[] args) {
		GraphDao dao = new TinkerMemoryDaoImpl();
		
		Event event = new Event();
		event.setSource(Source.Facebook());
		event.setContent("I'm loving this band at the WildHorse Saloon!");
		event.setCoordinate(new Coordinate(36.174465,-86.767960));
		event.setCreated(LocalDateTime.now());
		NamedEntity ne = new NamedEntity();
		ne.setName("WildHorse Saloon");
		List<NamedEntity> entities = new ArrayList<>();
		entities.add(ne);
		event.setEntities(entities);
		Party party = new Party();
		party.setName("Steve Jones");
		event.setParty(party);
		
		String eventId = dao.saveEvent(event);
		Iterator<Vertex> it = dao.getGraph().vertices(eventId);
		while(it.hasNext())
		{
			Vertex v = it.next();
			System.out.println(v.label() + ":" + v.id());
			for(String key : v.keys())
			{
				System.out.println("\t" + key + ": " + v.property(key).value());
			}
			System.out.println("\tEdges:");
			Iterator<Edge> edges = v.edges(Direction.OUT, "source", "by", "ner");
			while(edges.hasNext())
			{
				Edge e = edges.next();
				System.out.println("\t\t" + e.label() + ": " + e.inVertex().id());
			}
		}
		

	}

}
